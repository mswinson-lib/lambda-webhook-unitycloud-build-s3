# ucb-build_deploy_aws-s3

deploy Unity Cloud Build artifacts to AWS S3


## Installation

  
    git clone <this repo>


## Usage

**configuration**
  
    export AWS_ACCESS_KEY_ID=...
    export AWS_SECRET_ACCESS_KEY=...

**deployment**
  
    make build
    make deploy



## Contributing

