.PHONY: help server test build deploy

PACKAGE_NAME=ucb-build_deploy_aws-s3
STACK_NAME=webhook-ucbbuild


S3_BUCKET_NAME ?= $(PACKAGE_NAME)
ifdef S3_BUCKET_PREFIX
	S3_OPTIONS=--s3-bucket $(S3_BUCKET_NAME) --s3-prefix $(S3_BUCKET_PREFIX)
else
	S3_OPTIONS=--s3-bucket $(S3_BUCKET_NAME)
endif

BUILD_ENV ?= "sandbox"

ifeq ($(BUILD_ENV), "sandbox")
	CMD := docker run -it --rm \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v "$(PWD)":/var/opt \
		-p "3000:3000" \
		-e AWS_ACCESS_KEY_ID \
		-e AWS_SECRET_ACCESS_KEY \
		-e AWS_DEFAULT_REGION \
		cnadiminti/aws-sam-local
else
	CMD := docker run --rm \
		-v "$(PWD)":/var/opt \
		-e AWS_ACCESS_KEY_ID \
		-e AWS_SECRET_ACCESS_KEY \
		-e AWS_DEFAULT_REGION \
		cnadiminti/aws-sam-local
endif

help:
	@$(CMD)

ifeq ($(BUILD_ENV), "sandbox")
server:
	@$(CMD) local start-api --docker-volume-basedir "$(PWD)" --host 0.0.0.0
endif

test:
	@$(CMD) validate

build:
	@mkdir -p ./target
	@$(CMD) package --template-file ./template.yml --use-json --output-template-file target/$(PACKAGE_NAME).json $(S3_OPTIONS)

deploy:
	@$(CMD) deploy --template-file ./target/$(PACKAGE_NAME).json --stack-name $(STACK_NAME) $(S3_OPTIONS) --capabilities CAPABILITY_IAM
