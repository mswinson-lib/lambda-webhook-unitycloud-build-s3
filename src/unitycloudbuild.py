"""unitycloudbuild"""

import json
from collections import namedtuple


def _json_object_hook(data):
    return namedtuple('X', data.keys())(*data.values())

def _json2obj(data):
    return json.loads(data, object_hook=_json_object_hook)

class BuildStatus(object):
    """encapsulate build status"""

    def __init__(self, data):
        jsondata = json.dumps(data)

        self._data = _json2obj(jsondata)

    @property
    def project_name(self):
        """build status project name"""
        if hasattr(self._data, "projectName"):
            return self._data.projectName

        return ""

    @property
    def links(self):
        """build status links"""
        if hasattr(self._data, "links"):
            return self._data.links

        return namedtuple('links', [])

    @property
    def artifacts(self):
        """build status artifacts"""
        if hasattr(self.links, "artifacts"):
            return self.links.artifacts

        return []
