"""S3Uploader"""

import logging
import os
import tempfile

import boto3
import botocore

class S3Uploader(object):
    """upload artifact to s3 bucket"""

    _s3client = None
    _bucketname = ""
    _bucketprefix = ""

    @property
    def s3client(self):
        """s3client"""

        if self._s3client is None:
            self._s3client = boto3.resource('s3')

        return self._s3client

    def __init__(self, bucket, prefix, **kwargs):
        self._bucketname = bucket
        self._bucketprefix = prefix
        self._acl = 'public-read'

        self._logger = kwargs.get("logger", None)
        if self._logger is None:
            logging.basicConfig()
            self._logger = logging.getLogger("s3uploader")
            self._logger.setLevel(logging.INFO)

    def process(self, event):
        """process upload"""

        filename = getattr(event, "filename", None)
        if filename is None:
            return True

        bucketname = self._bucketname
        tempdir = tempfile.gettempdir()

        source = os.path.join(tempdir, filename)
        dest = os.path.join(self._bucketprefix, filename)

        self._logger.info("uploading " + source + " -> " + bucketname + "/" + dest)

        if not self._bucket_exists(bucketname):
            self._create_bucket(bucketname)

        self.s3client.Object(bucketname, dest).put(ACL=self._acl, Body=open(source, 'rb'))

        return False

    def _bucket_exists(self, name):
        try:
            self.s3client.meta.client.head_bucket(Bucket=name)
            return True
        except botocore.Exceptions.ClientError:
            return False

    def _create_bucket(self, name):
        self.s3client.create_bucket(Bucket=name)
