"""download support"""

import logging
import os
import os.path
import tempfile
import urllib2

class Downloader(object):
    """downloads files to temporary storage"""

    def __init__(self, tmpdir=None, **kwargs):
        if tmpdir is None:
            self._tempdir = tempfile.gettempdir()
        else:
            self._tempdir = tmpdir
            os.mkdir(self._tempdir)

        self._logger = kwargs.get("logger", None)
        if self._logger is None:
            logging.basicConfig()
            self._logger = logging.getLogger("downloader")
            self._logger.setLevel(logging.INFO)


    def process(self, event):
        """process download"""

        source = getattr(event, "href", None)
        filename = getattr(event, "filename", None)

        if source is None or filename is None:
            return True

        dest = os.path.join(self._tempdir, filename)

        self._logger.info("downloading " + source + " -> " + dest)

        fin = urllib2.urlopen(source)
        with(open(dest, 'wb')) as fout:
            fout.write(fin.read())

        return False
