from lambda_function import lambda_handler
import json


def test(event, context):
    body = {
            "links": {
                "artifacts": [
                    {
                        "files": []
                    }
                ]
            }
        }

    event = { "body": json.dumps(body) }
    lambda_handler(event, context)
