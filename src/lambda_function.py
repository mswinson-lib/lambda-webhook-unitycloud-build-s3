import json
from base64 import b64decode
import logging
import os
from downloader import Downloader
from s3uploader import S3Uploader
import unitycloudbuild

def data_for(event):
    body = event.get("body", "")
    data = json.loads(body)

    return data

def lambda_handler(event, context):
    # initialize logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    if os.environ.get('DEBUG', None) is not None:
        logger.setLevel(logging.DEBUG)
        logger.info("setting logger to DEBUG mode")

    # initialize build status
    data = data_for(event)
    logger.debug(data)
    buildstatus = unitycloudbuild.BuildStatus(data)

    # initialize downloader
    downloader = Downloader(logger=logger)

    # initialize uploader
    bucketname = os.environ.get('S3_BUCKETNAME', '')
    bucketprefix = os.environ.get('S3_BUCKET_PREFIX', '')

    project_name = buildstatus.project_name
    prefix = bucketprefix + "/" + project_name
    uploader = S3Uploader(bucketname, prefix, logger=logger)

    # pipeline
    for artifact in buildstatus.artifacts:
        files = getattr(artifact, "files", [])

        for file in files:
            downloader.process(file)
            uploader.process(file)
